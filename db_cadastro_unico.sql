﻿-- ** SCHEMA: cacesso *******************
CREATE SCHEMA cacesso;
COMMENT ON SCHEMA cacesso IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito a mecanismo de controle de acesso, seja autenticação, seja autorizacao.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.grupo 
(
	id bigserial NOT NULL UNIQUE,
	grupo character varying(30) NOT NULL,
	situacao character varying(20) NOT NULL,
	
	primary key (id)
);
COMMENT ON TABLE cacesso.grupo IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "entidade que reune os grupos de usuarios dos sistemas da prefeitura. o grupo eh elemento definidor de um conjunto de competencias de acesso para persistir e/ou ler dados definidos pelas entidades de dominio.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.grupo.situacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este atributo deve ser utilizado com a funcao de marcar a instancia de grupo como estando ativa ou nao para uso do sistema, de forma que desabilita ou habilita todos os usuarios associados a esta instancia de grupo.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.http_method 
(
	id bigserial NOT NULL UNIQUE,
	http_method character varying(10) NOT NULL,
	
	primary key (id)
);
COMMENT ON TABLE cacesso.http_method IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "o controle de acesso aos dados das entidades de dominio do si eh efeito, entre outras coisas, pelo tipo de mensagem http enviada ao servidor do si (no caso o datasource).", "DataDeImplantacao":""}';

CREATE TABLE cacesso.usuario 
(
	id bigserial NOT NULL UNIQUE,
	nome character varying(120) NOT NULL,
	cpf character varying(16) NOT NULL,
	login character varying(25) NOT NULL,
	password character varying(55) NOT NULL,
	email character varying(100) NOT NULL,
	situacao boolean NOT NULL default false, 
	
	primary key (id)
);
COMMENT ON TABLE cacesso.usuario IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "entidade que armazena dados de usuarios que deverao realizar processos de autenticacao e autorizacao nos sis da prefeitura.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.usuario.situacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este atributo deve ser utilizado com a funcao de marcar a instancia de usuario como estando ativa ou nao para uso do sistema.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.uri 
(
	id bigserial NOT NULL UNIQUE,
	uri character varying(600) NOT NULL,
	
	primary key (id)
);
COMMENT ON TABLE cacesso.uri IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados das uris para que junto com dados de usuarios e metodos_https realizar controle de acesso aos dados dos sis da prefeitura.", "DataDeImplantacao":""}';
 
CREATE TABLE cacesso.lotacao 
(
	id bigserial NOT NULL UNIQUE,
	usuario_id bigint NOT NULL,
	grupo_id bigint NOT NULL,
	situacao integer NOT NULL,
	data date NOT NULL,
	
	primary key (id),
	foreign key (usuario_id) references cacesso.usuario (id) on update cascade on delete cascade,
	foreign key (grupo_id) references cacesso.grupo (id) on update cascade on delete cascade
);
COMMENT ON TABLE cacesso.lotacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados de lotacao de usuarios em grupos.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.lotacao.situacao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este atributo deve ser utilizado com a funcao de marcar a instancia de lotacao ao estado da lotacao do usuario relativo a um grupo para utilizacao do si.", "DataDeImplantacao":""}';

CREATE TABLE cacesso.permissao 
(
	id bigserial NOT NULL UNIQUE,
	grupo_id bigint NOT NULL,
	uri_id bigint NOT NULL,
	http_method_id bigint NOT NULL,
	permissao character varying(12) NOT NULL,
	
	primary key (id),
	foreign key (grupo_id) references cacesso.grupo (id) on update cascade on delete cascade,
	foreign key (uri_id) references cacesso.uri (id) on update cascade on delete cascade
);
COMMENT ON TABLE cacesso.permissao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados das permissoes relativas a triade: uri, grupo e metodo http. isso auxiliarah a realizar controle de acesso aos dados dos sis da prefeitura.", "DataDeImplantacao":""}';
COMMENT ON COLUMN cacesso.permissao.permissao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "os valores da permissao dizem respeito a possibilidade de usuario do sistema, habilitado a usar, pode ler ou alterar estado dos dados nas entidades de dados do si.", "DataDeImplantacao":""}';


-- ** SCHEMA: pessoal *******************
CREATE SCHEMA pessoal;
COMMENT ON SCHEMA pessoal IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema de pessoal (que tipifiquem pessoas de natureza fisica ou juridica), seja autenticação, seja autorizacao.", "DataDeImplantacao":""}';

CREATE TABLE pessoal.pessoa 
(
	id bigserial NOT NULL UNIQUE,
	data_de_nascimento date,
	nome character varying(120) NOT NULL,
	end_logradouro character varying(120) NOT NULL,
	end_numero character varying(20) NOT NULL,
	end_bairro character varying(120) NOT NULL,
	end_cidade character varying(120) NOT NULL,
	end_uf character varying(2) NOT NULL,
	end_cep character varying(12),
	end_complemento character varying(300),
	telefone_fixo character varying(15),
	telefone_movel character varying(15),
	email character varying(200),
	im character varying(200),
	log_user bigint,
	log_date date,
 	
	primary key (id)
);
COMMENT ON TABLE pessoal.pessoa IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "armazenar dados de municipes, funcionarios, pessoas fisicas e pessoas juridicas de forma geral para o municipio.", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.pessoa.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.pessoa.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
 
CREATE TABLE pessoal.pessoa_fisica 
(
	id bigint NOT NULL UNIQUE,
	cor character varying(20),
	foto character varying(520),
	grau_de_instrucao character varying(20),
	rc_data_do_registro date,
	rc_folha_do_livro character varying(10),
	rc_nome_do_cartorio character varying(120),
	rc_nome_do_livro character varying(120),
	rc_numero character varying(20),
	religiao character varying(40),
	sexo boolean,
	tipo_sanguineo character varying(5),
	necessidade_especial character varying(20),
	resp_responsavel character varying(120),
	resp_mae character varying(120),
	resp_pai character varying(120),
	resp_cpf character varying(16),
	
	primary key (id),
	foreign key (id) references pessoal.pessoa (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.pessoa_fisica IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa. esta entidade eh uma especializacao daquela.", "DataDeImplantacao":""}';

CREATE TABLE pessoal.pessoa_juridica 
(
	id bigint NOT NULL UNIQUE,
	cnpj character varying(16) NOT NULL UNIQUE,
	inscricao_estadual character varying (36),
	inscricao_municipal character varying (36),
	razao_social character varying(120) NOT NULL,
	status boolean NOT NULL default true,
	
	primary key (id),
	foreign key (id) references pessoal.pessoa (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.pessoa_juridica IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa. esta entidade eh uma especializacao daquela.", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.pessoa_juridica.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';

CREATE TABLE pessoal.cidadao 
(
	id bigint NOT NULL UNIQUE,
	cpf character varying(16) NOT NULL UNIQUE,
	estado_civil character varying(20),
	nacionalidade character varying(30),
	naturalidade character varying(30),
	pis_pasep character varying(30),
	cm_categoria character varying(20),
	cm_data_de_emissao date,
	cm_numero character varying(20),
	ct_data_de_emissao date,
	ct_numero character varying(28),
	ct_serie character varying(20),
	rg_data_de_emissao date,
	rg_numero character varying(16),
	rg_orgao_expeditor character varying(20),
	rg_uf character varying(2),
	te_numero character varying(20),
	te_secao character varying(12),
	te_zona character varying(12),
	fp_formacao character varying(20),
	fp_numero_de_ordem_do_conselho character varying(20),
	fp_grau_de_instrucao character varying(12),
	fp_profissao character varying(20),
	fp_conselho character varying(20),
	cnh_numero character varying(20),
	cnh_categoria character varying(26),
	cnh_validade date,
	ss_nis character varying(30) UNIQUE,
	ss_numero_caixa character varying(16),
	ss_numero_registro character varying(20),
	status boolean NOT NULL default true,
	version integer,
	
	primary key (id),
	foreign key (id) references pessoal.pessoa_fisica (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.cidadao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa_fisica. esta entidade eh uma especializacao daquela.", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.cidadao.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';

CREATE TABLE pessoal.funcionario 
(
	id bigserial NOT NULL UNIQUE,
	cidadao_id bigint NOT NULL,
	data_de_admissao date,
	data_de_demissao date,
	matricula character varying NOT NULL UNIQUE,
	end_logradouro character varying(120),
	end_numero character varying(10),
	end_bairro character varying(120),
	end_cidade character varying(120),
	end_uf character varying(2),
	end_cep character varying(12),
	end_complemento character varying(300),
	telefone_fixo character varying(15),
	telefone_movel character varying(15),
	email character varying(200),
	im character varying(120),
	tipo_de_contrato character varying,
	validado boolean NOT NULL default false,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	version integer,
	
	primary key (id),
	foreign key (cidadao_id) references pessoal.cidadao (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.funcionario IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descrever cidadao pela sua perspectiva de relacao com o agente empregador, no caso, a prefeitura", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.funcionario.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.funcionario.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.funcionario.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE pessoal.prefeitura 
(
	id bigint NOT NULL UNIQUE,
	brasao character varying(520),
	status boolean NOT NULL default true,
	
	primary key (id),
	foreign key (id) references pessoal.pessoa_juridica (id) on update cascade on delete cascade
);
COMMENT ON TABLE pessoal.prefeitura IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "ver finalidade de pessoal.pessoa_juridica. esta entidade eh uma especializacao daquela", "DataDeImplantacao":""}';
COMMENT ON COLUMN pessoal.prefeitura.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';

-- ** SCHEMA: organograma ****************
CREATE SCHEMA organograma;
COMMENT ON SCHEMA organograma IS '{"Autor": "Thiago", "DataDeCriacao": "20151008", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema organizacional da prefeitura. avaliar o que diz a legislacao a respeito.", "DataDeImplantacao":""}';

CREATE TABLE organograma.cargo
(
	id bigserial NOT NULL,
	cargo character varying(120) NOT NULL UNIQUE,
	moeda_salario double precision,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE organograma.cargo IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.cargo.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.cargo.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.cargo.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE organograma.celula
(
	id bigserial NOT NULL,
	subordinacao bigint,
	celula character varying(60) NOT NULL,
	telefone_fixo character varying(13),
	abreviatura character varying(20),
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	version integer,
	
	primary key (id),
	foreign key (subordinacao) references organograma.celula (id) on update cascade on delete cascade
);
COMMENT ON TABLE organograma.celula IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.celula.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.celula.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.celula.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE organograma.modalidade_de_funcao
(
	id bigserial NOT NULL,
	modalidade character varying(120) NOT NULL UNIQUE,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE organograma.modalidade_de_funcao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.modalidade_de_funcao.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.modalidade_de_funcao.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.modalidade_de_funcao.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE organograma.funcao
(
	id bigserial NOT NULL,
	modalidade_de_funcao_id bigint NOT NULL,
	funcao character varying(60) NOT NULL UNIQUE,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (modalidade_de_funcao_id) references organograma.modalidade_de_funcao (id) ON UPDATE no action ON DELETE no action
);
COMMENT ON TABLE organograma.funcao IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.funcao.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.funcao.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN organograma.funcao.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

-- ** SCHEMA: documento
CREATE SCHEMA documento;
COMMENT ON SCHEMA documento IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito aos documentos da prefeitura.", "DataDeImplantacao":""}';

CREATE TABLE documento.tipo_de_documento 
(
	id bigserial NOT NULL UNIQUE,
	tipo character varying(30) NOT NULL UNIQUE,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE documento.tipo_de_documento IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve os tipos de documentos utilizados pela prefeitura.", "DataDeImplantacao":""}';
COMMENT ON COLUMN documento.tipo_de_documento.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN documento.tipo_de_documento.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN documento.tipo_de_documento.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE documento.documento 
(
	id bigserial NOT NULL UNIQUE,
	tipo_de_documento_id bigint NOT NULL,
	identificador character varying(36) NOT NULL UNIQUE,
	documento_dir character varying(520),
	data_de_criacao date NOT NULL,
	data_de_publicacao date,
	objeto text,
	matricula_do_responsavel character varying (120),
	formato_do_documento character varying (10) NOT NULL,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	autor bigint NOT NULL,
	version integer,
	
	primary key (id),
	foreign key (tipo_de_documento_id) references documento.tipo_de_documento (id) on update cascade on delete cascade,
	foreign key (autor) references pessoal.cidadao (id) on update cascade on delete cascade
);
COMMENT ON TABLE documento.documento IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve os documentos utilizados pela prefeitura.", "DataDeImplantacao":""}';
COMMENT ON COLUMN documento.documento.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN documento.documento.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN documento.documento.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

-- ** SCHEMA: convenios *******************
CREATE SCHEMA convenio;
COMMENT ON SCHEMA convenio IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema de convenios celebrados pela prefeitura com instituicoes da sociedade.", "DataDeImplantacao":""}';

CREATE TABLE convenio.modalidade 
(
	id bigserial NOT NULL UNIQUE,
	modalidade character varying(36) NOT NULL UNIQUE,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE convenio.modalidade IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve as modalidades de convenios instituidas pela legislacao vigente.", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.modalidade.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.modalidade.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.modalidade.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
 
CREATE TABLE convenio.convenio 
(
	id bigserial NOT NULL UNIQUE,
	modalidade_id bigint NOT NULL,
	identificador_de_convenio character varying(36) NOT NULL UNIQUE,
	data_de_inicio date NOT NULL,
	data_de_termino date,
	objeto text NOT NULL,
	valor character varying (20) NOT NULL,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (modalidade_id) references convenio.modalidade (id) on update cascade on delete cascade
);
COMMENT ON TABLE convenio.convenio IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "descreve as modalidades de convenios instituidas pela legislacao vigente.", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.documento_id IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "especificar qual foi o documento, EDITAL, que deu origem a celebracao do convenio", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE convenio.conveniado 
(
	id bigserial NOT NULL UNIQUE,
	celula_id bigint NOT NULL,
	pessoa_juridica_id bigint NOT NULL,
	convenio_id bigint NOT NULL,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (celula_id) references organograma.celula (id) on update cascade on delete cascade,
	foreign key (pessoa_juridica_id) references pessoal.pessoa_juridica (id) on update cascade on delete cascade,
	foreign key (convenio_id) references convenio.convenio (id) on update cascade on delete cascade
);
COMMENT ON TABLE convenio.conveniado IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "entidade cuja finalidade eh capturar os dados das relacoes de convenios celebrados pelo municipio", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.conveniado.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.conveniado.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.conveniado.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE convenio.convenio_documento 
(
	id bigserial NOT NULL UNIQUE,
	documento_id bigint NOT NULL,
	convenio_id bigint NOT NULL,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (documento_id) references documento.documento (id) on update cascade on delete cascade,
	foreign key (convenio_id) references convenio.convenio (id) on update cascade on delete cascade
);
COMMENT ON TABLE convenio.convenio_documento IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "auto evidente", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio_documento.status IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio_documento.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN convenio.convenio_documento.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

-- ** SCHEMA: contrato *******************
CREATE SCHEMA contrato;
COMMENT ON SCHEMA contrato IS '{"Autor": "Thiago", "DataDeCriacao": "20151008", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema de contratos celebrados pela prefeitura com instituicoes da sociedade.", "DataDeImplantacao":""}';

CREATE TABLE contrato.contrato 
(
	id bigserial NOT NULL UNIQUE,
	identificador_do_processo character varying(30) NOT NULL UNIQUE,
	identificador_do_contrato character varying(30) NOT NULL UNIQUE,
	data_de_inicio date NOT NULL,
	data_de_termino date,
	valor_do_contrato float NOT NULL,
	vinculo_oracamentario character varying(36) NOT NULL,
	projeto_de_atividade character varying(36) NOT NULL,
	exercicio int NOT NULL,
	fundamentacao_legal text NOT NULL,
	objeto text NOT NULL,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE contrato.contrato IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "descreve as varias instancias de contratos celebrados pelo municipio.", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.identificador_do_processo IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.identificador_do_contrato IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.vinculo_oracamentario IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.projeto_de_atividade IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.status IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.log_user IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato.log_date IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE contrato.contratada 
(
	id bigserial NOT NULL UNIQUE,
	celula_id bigint NOT NULL,
	pessoa_juridica_id bigint NOT NULL,
	contrato_id bigint NOT NULL,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (celula_id) references organograma.celula (id) on update cascade on delete cascade,
	foreign key (pessoa_juridica_id) references pessoal.pessoa_juridica (id) on update cascade on delete cascade,
	foreign key (contrato_id) references contrato.contrato (id) on update cascade on delete cascade
);
COMMENT ON TABLE contrato.contratada IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "entidade cuja finalidade eh capturar os dados das relacoes de contratos celebrados pelo municipio", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contratada.status IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contratada.log_user IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contratada.log_date IS '{"Autor": "Thiago", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE contrato.contrato_documento (
	id bigserial NOT NULL UNIQUE,
	documento_id bigint NOT NULL,
	contrato_id bigint NOT NULL,
	status boolean NOT NULL default false,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (documento_id) references documento.documento (id) on update cascade on delete cascade,
	foreign key (contrato_id) references contrato.contrato (id) on update cascade on delete cascade
);
COMMENT ON TABLE contrato.contrato_documento IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "auto evidente", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato_documento.status IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato_documento.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN contrato.contrato_documento.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151021", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

-- ** SCHEMA: admrh *********************
CREATE SCHEMA admrh;
COMMENT ON SCHEMA admrh IS '{"Autor": "Thiago", "DataDeCriacao": "20151008", "Finalidade": "este esquema deve agrupar todas as entidades que dizem respeito ao esquema ao modo de gestao da prefeitura, controle de rh.", "DataDeImplantacao":""}';

CREATE TABLE admrh.local_de_trabalho
(
	id bigserial NOT NULL,
	local_de_trabalho character varying(120) NOT NULL UNIQUE,
	telefone_fixo character varying(13),
	abreviatura character varying(20),
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id)
);
COMMENT ON TABLE admrh.local_de_trabalho IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.local_de_trabalho.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.local_de_trabalho.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.local_de_trabalho.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE admrh.lotacao_de_pessoal
(
	id bigserial NOT NULL,
	cargo_id bigint,
	celula_id bigint NOT NULL,
	funcao_id bigint,
	funcionario_id bigint NOT NULL,
	local_de_trabalho_id bigint,
	data_de_inicio date,
	data_de_termino date,
	situacao character varying(20),
	carga_horaria character varying(10),
	observacao character varying(400),
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (cargo_id) references organograma.cargo (id) on update cascade on delete cascade,
	foreign key (celula_id) references organograma.celula (id) on update cascade on delete cascade,
	foreign key (funcao_id) references organograma.funcao (id) on update no action on delete no action,
	foreign key (funcionario_id) references pessoal.funcionario (id) on update no action on delete no action,
	foreign key (local_de_trabalho_id) references admrh.local_de_trabalho (id) on update no action on delete no action
);
COMMENT ON TABLE admrh.lotacao_de_pessoal IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.lotacao_de_pessoal.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.lotacao_de_pessoal.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.lotacao_de_pessoal.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';

CREATE TABLE admrh.vinculo_externo
(
	id bigserial NOT NULL,
	funcionario_id bigint NOT NULL,
	carga_horaria character varying(10),
	local_de_trabalho character varying(120) NOT NULL,
	situacao character varying(20),
	data_de_inicio date,
	data_de_termino date,
	status boolean NOT NULL default true,
	log_user bigint,
	log_date date,
	
	primary key (id),
	foreign key (funcionario_id) references pessoal.funcionario (id) on update cascade on delete cascade
);
COMMENT ON TABLE admrh.vinculo_externo IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.vinculo_externo.status IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "status, eh um atributo que eh utilizado para referenciar a condicao de exclusao desta instancia de dado desta entidade. o valor para tanto deve ser atribuido como false", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.vinculo_externo.log_user IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_user, eh um atributo que eh utilizado para referenciar a identificacao que diz respeito a qual usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
COMMENT ON COLUMN admrh.vinculo_externo.log_date IS '{"Autor": "Julio", "DataDeCriacao": "20151005", "Finalidade": "log_date, eh um atributo que eh utilizado para referenciar a data na qual o usuario realizou operacao sobre esta instancia de dado", "DataDeImplantacao":""}';
